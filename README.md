# 7D2DMods

## No Zombies

Yes, it sounds counterproductive to buy a zombie game and then remove all zombies but some people realy like survival games. They just don't want to see zombies in it. I would love to create a model to replace the zombies but for now I just replaced all zombie spawns with zombie dogs. Also adjusted the dogs to be slower during the day and act more like zombies would, like tear down your house and other fun stuff.

http://7d2dmodlauncher.org